/**
 * 
 */
package podam;

import java.lang.reflect.Type;
import java.util.Map;

import uk.co.jemos.podam.api.AttributeMetadata;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.typeManufacturers.StringTypeManufacturerImpl;

/**
 * @author fernando
 *
 */
public class QuestionStringTypeManufacturer extends StringTypeManufacturerImpl {

	@Override
	public String getType(DataProviderStrategy dataProviderStrategy
			, AttributeMetadata attributeMetadata
			, Map<String, Type> genericTypesArgumentsMap) {
		if (Question.class.isAssignableFrom(attributeMetadata.getPojoClass())) {

            if ("question".equals(attributeMetadata.getAttributeName())) {
                    return "¿Pregunta de prueba?";
            } else if ("help".equals(attributeMetadata.getAttributeName())) {
                    return "Responda la pregunta.";
            }
	    }
	    return super.getType(dataProviderStrategy, attributeMetadata, genericTypesArgumentsMap);
	}

}

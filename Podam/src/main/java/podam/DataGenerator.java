/**
 * 
 */
package podam;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONWriter;

import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

/**
 * @author fernando
 *
 */
public class DataGenerator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PodamFactory factory = new PodamFactoryImpl();
//		
//		TestPOJO myTestPojo = factory.manufacturePojo(TestPOJO.class);
//		
//		System.out.println(myTestPojo.toString());
		
		QuestionStringTypeManufacturer questionStringTypeManufacturer = new QuestionStringTypeManufacturer();
		
		factory.getStrategy().addOrReplaceTypeManufacturer(String.class, questionStringTypeManufacturer);
		
		@SuppressWarnings("unchecked")
		List<Question> list = factory.manufacturePojo(ArrayList.class, Question.class);
		
		System.out.println(JSONWriter.valueToString(list));

	}

}

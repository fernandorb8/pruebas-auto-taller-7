/**
 * 
 */
package podam;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author mtedone https://mtedone.github.io/podam/index.html
 *
 */
public class TestPOJO {
    private String randomString;
    private String postCode;

    public String getRandomString() { return randomString; }

    public void setRandomString(String randomString) { this.randomString = randomString; }

    public String getPostCode() { return postCode; }

    public void setPostCode(String postCode) { this.postCode = postCode; }
    
    @Override
    public String toString() {
    	return "randomString " + getRandomString() + "\n" 
    			+ "postCode " + getPostCode();
    }
}

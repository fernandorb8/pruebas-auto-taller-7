﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mockaroo
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RestClient rclient = new RestClient();
            rclient.endPoint = url.Text;
            debugOutput("Rest con Mockaroo Creado");
            string response = string.Empty;
            response = rclient.makeRequest();
            debugOutput(response);
        }

        private void debugOutput(string debugText) {
            try
            {
                System.Diagnostics.Debug.Write(debugText + Environment.NewLine);
                resultado.Text = resultado.Text + debugText + Environment.NewLine;
                resultado.SelectionStart = resultado.Text.Length;
                //resultado.AppendText(resultado.Text);
            }
            catch {

            }

        }
    }
}
